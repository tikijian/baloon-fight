﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch(collision.gameObject.tag)
        {
            case "Player":
                PlayerController player = collision.GetComponent<PlayerController>();
                player.Die();
                break;
        }
    }
}
