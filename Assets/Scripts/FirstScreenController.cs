﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstScreenController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            StartCoroutine(LoadLevelScene());
        }
    }

    IEnumerator LoadLevelScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Scenes/Level1");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
