﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    const int UNCOLLIDABLE_LAYER = 11;
    Rigidbody2D body;
    Animator animator;
    SpriteRenderer sprite;

    public float runSpeed;
    public float jumpForce;
    public float maxVelocity;
    public float deathPushForce;
    public int lives;
    
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        processKeyDown();
        processKeyHold();
        animator.SetFloat("VelocityX", Mathf.Abs(body.velocity.x));
        animator.SetFloat("VelocityY", body.velocity.y);
    }

    void processKeyDown()
    {
        if (lives == 0) return;
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            body.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            sprite.flipX = false;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            sprite.flipX = true;
        }
    }

    void processKeyHold()
    {
        if (lives == 0) return;
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            body.AddForce(-transform.right * runSpeed);
        } else if (Input.GetKey(KeyCode.RightArrow))
        {
            body.AddForce(transform.right * runSpeed);
        }
    }

    public void  OnHitByEnemy()
    {
        lives--;
        if (lives <= 0)
        {
            Die();
        } else
        {
            animator.SetLayerWeight(animator.GetLayerIndex("OneBaloon"), 1);
        }
    }

    public void Die()
    {
        gameObject.layer = UNCOLLIDABLE_LAYER;
        body.AddForce(new Vector2(0, deathPushForce), ForceMode2D.Impulse);
        animator.SetBool("IsDying", true);
        Destroy(gameObject, 3);
    }
}
