﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaloonColliderController : MonoBehaviour
{
    public GameObject parent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
;       switch (collision.gameObject.tag)
        {
            // Enemy hits palyer's baloon
            case "Enemy":
                parent.GetComponent<PlayerController>().OnHitByEnemy();
                break;
            case "Player":
                parent.GetComponent<EnemyController>().OnHitByPlayer();
                break;
        }
    }
}
