﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    const int UNCOLLIDABLE_LAYER = 11;
    Rigidbody2D body;
    Animator animator;
    SpriteRenderer sprite;
    enum State {PUMPING, PLAYING, DYING};
    State currentState = State.PUMPING;

    public float runSpeed;
    public float jumpForce;
    public float deathPushForce;
    public GameObject pumpingBaloon;

    bool isFlying = false;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();

        animator.SetInteger("State", (int)currentState);
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case State.PLAYING:
                UpdateWhilePlaying();
                break;
            case State.DYING:
                break;
            default:
                break;
        }
    }

    void UpdateWhilePlaying()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            body.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            sprite.flipX = false;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            sprite.flipX = true;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            body.AddForce(-transform.right * runSpeed);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            body.AddForce(transform.right * runSpeed);
        }

        animator.SetFloat("VelocityX", Mathf.Abs(body.velocity.x));
        animator.SetFloat("VelocityY", body.velocity.y);
    }

    public void OnPumpingEnd()
    {
        currentState = State.PLAYING;
        Destroy(pumpingBaloon);
        animator.SetInteger("State", (int)currentState);
    }

    public void OnHitByPlayer()
    {
        Debug.Log("Hit by Player");
    }
}
